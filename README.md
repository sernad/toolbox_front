# Interfaz REACT

Interfaz web correspondiente al desafio para Toolbox, consta de dos componente que hace una llamada a una API y muestra la lista de datos retornado.

La petición es realizada a traves de redux, donde se maneja el estado global tanto de la lista de archivos como el estatus de la peticion (cargando / no cargando)

Los estilos son manejados a traves de sass

