import React, {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Header from '../components/fileList/header';

import './styles.scss';
import { getListData } from "../store/actions/List";


const FileTable = () => {

  const stateData  = useSelector(state => state);
  const dispatch = useDispatch();
  
  useEffect(() => {
    (async ()=>{ dispatch(await getListData()); })()
  }, [dispatch]);

  return(
    
    <div className='listWrapper'>

      <Header />
      {stateData.loading && <div className='blink'> Loading</div> }
      {!stateData.loading && stateData.list.length === 0 && <div className='message'> The list is empty</div> }


      {stateData.list.length > 0 && !stateData.loading && 
      
        <table className='fileListTable'>
          <thead>
            <tr>
              <th scope="col">File Name</th>
              <th scope="col">Text</th>
              <th scope="col">Number</th>
              <th scope="col">Hex</th>
            </tr>
          </thead>
          <tbody>
            { 
              stateData.list.map((item, index) => (
                <tr key={index}>
                  <td> {item.filename} </td>
                  <td> {item.text} </td>
                  <td> {item.number} </td>
                  <td> {item.hex} </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      }
      


    </div>
  )
}

export default FileTable