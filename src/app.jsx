import React from 'react';

import { CssBaseline } from '@material-ui/core';
import Container from './components/wrappers/mainContainer'
import {Provider}    from "react-redux";
import store from "./store/ConfigureStore";

const App = () =>{
    
  return (
    <Provider store={store}> 
        <Container/>
        <CssBaseline />
    </Provider>
  )
};

export default App;
