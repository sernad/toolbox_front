import React from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';


const Header = () =>{
  const customStyles = useStyle();

  return (
    <Grid className={customStyles.content} container alignItems="center">

      <Grid item xs={10}>
        <span>React Test App</span>
      </Grid>
    </Grid>
  )
}

export default Header


const useStyle = makeStyles(theme => ({
  content: {
    backgroundColor: '#ff6666',
    color: '#fff',
    fontWeight: 'bold',
    border: '1px solid #000',
    fontSize: '16pt',
    padding: '10px 0'
  }
}));