import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import {makeStyles} from '@material-ui/core';


import Main from '../../navigation/mainNavigation';

const useStyle = makeStyles(theme=>({
  root: {
    display: 'flex',
    height:'100%'
  }
}))

const AuthContainer = () => {
  const customStyles = useStyle();

  return (
    <Router>
      <div className={customStyles.root}>
        <Main />
      </div>
    </Router>
  )
}

export default AuthContainer;
