import { SET_lIST, SET_LOADING} from './ActionTypes'


import { enviroment } from '../../config/enviroment';
import axios from 'axios';

const API_URL = enviroment.API_URL

const fetchList = async() =>{
  return new Promise ( (resolve, reject) => {

    axios.get(`${API_URL}/files/list`).then( ({data}) => {

      const finalItems = [];
      data.forEach(item => {
        const testing = item.lines.map((line, index) => ( { id: index, filename: item.file, text: line.text, number: line.number, hex: line.hex} ));
        finalItems.push(...testing);
      })

      resolve(finalItems)
    }).catch(err => {
      reject(err)
    })
  })
}

// export const getListData = async() => {
//   try {
//     const list = await fetchList();
//     return({type: SET_lIST, list })
//   } catch (error) {
//     console.log('....');
//   }
// };


export const getListData = async() => {
  return async(dispatch) =>{
    try {
      dispatch({type: SET_LOADING, loading: true })
      const list = await fetchList();
      dispatch({type: SET_lIST, list })
      dispatch({type: SET_LOADING, loading: false })
      // false
    } catch (error) {
      console.log(error);
    }
  }
};
