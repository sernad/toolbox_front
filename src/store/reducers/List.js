import * as types from "../actions/ActionTypes";

const initialState = {
  list  : [],
  loading : false
};


const loadingReducer = (state = initialState, action) => {
  
  switch (action.type) {
    
    case types.SET_lIST:
      return{
        ...state,
        list: action.list
      }
    
    case types.SET_LOADING:
      return{
        ...state,
        loading: action.loading
      }
    
    default:
      return state;
  }
}

export default loadingReducer